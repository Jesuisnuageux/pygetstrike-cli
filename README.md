### How to use that thing ? ###

Set execution right
```
$ chmod +x ./strike-cli.py
```

This script need a query as parameters
```
$ ./strike-cli.py citizenfour
```

You end in front of a
```
[cmd]>
```
when you can start doing something !
First of all 'help' gives you all commands you can use
```
[cmd]>help
List of commands :
- help : print this help
- info [torrent hash] : print info about a specific torrent from its hash
- info [index] : print info about a specific from its position in the results list
- info [index debut]-[index fin] : print info about a range of torrent from their position in results list
- search [query] : use getstrike.net browser engine to get a list of torrent matching the query
- download [torrent hash] : download the .torrent file from its hash
- download [index] : download the .torrent file from its position in the results list
- start [index] : start a torrent client and add the torrent file to its download list from its position in results list
```

you can start browsing your results by printing the 10th first results :
```
[cmd]>info 0-10
Result num : 0

		title : Citizenfour 2014 HDTV x264-BATV[ettv]
		hash : 81A29061420DBD41CFE9565BD7B2E4D447D411E8
		category : TV
		subcategory : 
		seeds : 282
		leeches : 35
		file_count : 3
		size : 502.37 MB
		upload date : Feb 24, 2015
		uploader username : ettv
[... I won't copy all results ...]
Result num : 9

		title : Citizenfour (2014) 720p WEB-DL AAC x264 - LOKI
		hash : A0C27D1D213A9996AB036F2317C000A5DC41B03F
		category : Movies
		subcategory : 
		seeds : 24
		leeches : 10
		file_count : 3
		size : 899.16 MB
		upload date : Jan 21, 2015
		uploader username : LOKI-Torrents
```

And I'll go for the 10th one, so :
```
[cmd]>download 9
```

that's it :)