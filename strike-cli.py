#!/usr/bin/python

"""
    Author : jesuisnuageux
    Date : 21/03/2015
    Function : A simple console client to browse and download torrent using getstrike.net API and my little Python wrapper
"""

"""
    TODO:
        > Add sorting support > Changes in wrapper needed
"""

import pygetstrike as gsk
import argparse as parse
from os import path
from subprocess import Popen

"""
    Used to find the path to your torrent client if it exists
     if you got one and its path isn't in the list below it'd be nice to e-mail me your torrent client path
     so I could add it to the current list.
     And for sure, you can patch it by yourself just by adding it to this same list :)
"""
LIST_OF_KNOWN_TORRENT_CLIENT = ["/usr/bin/qbittorrent", "/usr/bin/transmission-qt"]
TORRENT_CLIENT_PATH = None
for torrent_client_path in LIST_OF_KNOWN_TORRENT_CLIENT:
    if path.exists("{}".format(torrent_client_path)):
        TORRENT_CLIENT_PATH = torrent_client_path


class App:
    def __init__(self, query, verbose, sort):
        self.query = query
        self.verbose = verbose
        self.sort = sort
        self.torrent_client_path = TORRENT_CLIENT_PATH
        self.background_process = []
        if self.verbose:
            print("Verbose output is set")
        self.api = gsk.Api(self.verbose)

    def search(self):
        self.api.search(self.query)
        if self.verbose:
            print("API returned {} torrent(s)".format(len(self.api.results)))

    def run(self):
        if self.torrent_client_path == None:
            print("/!\\ start command disable since we're unable to find where's your torrent client")
        self.search()
        self.IOhandler()

    def printHelp(self):
        print("List of commands :")
        print("- help : print this help")
        print("- info [torrent hash] : print info about a specific torrent from its hash")
        print("- info [index] : print info about a specific from its position in the results list")
        print(
            "- info [index debut]-[index fin] : print info about a range of torrent from their position in results list")
        print("- search [query] : use getstrike.net browser engine to get a list of torrent matching the query")
        print("- download [torrent hash] : download the .torrent file from its hash")
        print("- download [index] : download the .torrent file from its position in the results list")
        print(
            "- start [index] : start a torrent client and add the torrent file to its download list from its position in results list")

    def killBackgroundProcess(self):
        if self.verbose:
            print("Killing remaining background processes")
        for process in self.background_process:
            process.kill()
        for process in self.background_process:
            process.wait()

    def IOhandler(self):
        cmd = ""
        while not cmd.lower() in ["exit", "quit", "bye"]:
            cmd = input("[cmd]>").lower()
            # -- HELP COMMAND
            if cmd.split(' ')[0] == "help":
                self.printHelp()
            # -- INFO COMMAND
            elif cmd.split(' ')[0] == "info":
                if len(cmd.split(' ')[1]) >= 40:
                    # get torrent info
                    self.api.getInfo(cmd.split(' ')[1])
                    print(self.api.results[0].info())
                elif '-' in cmd.split(' ')[1]:
                    if cmd.split(' ')[1].split('-')[0].isnumeric() and cmd.split(' ')[1].split('-')[1].isnumeric():
                        for i in range(
                                abs(int(cmd.split(' ')[1].split('-')[1]) - int(cmd.split(' ')[1].split('-')[0]))):
                            print("Result num : {}".format(int(cmd.split(' ')[1].split('-')[0]) + i))
                            print(self.api.results[int(cmd.split(' ')[1].split('-')[0]) + i].info())
                elif cmd.split(' ')[1].isnumeric():
                    if int(cmd.split(' ')[1]) >= 0 and int(cmd.split(' ')[1]) < len(self.api.results):
                        print("Result n°{}".format(cmd.split(' ')[1]))
                        print(self.api.results[int(cmd.split(' ')[1])].info())
                    else:
                        print("/!\\ Illegal argument /!\\")
                else:
                    print("/!\\ Illegal argument /!\\")
            # -- SEARCH COMMAND
            elif cmd.split(' ')[0] == "search":
                self.query = ' '.join(cmd.split(' ')[1:])
                self.search()
            # -- DOWNLOAD COMMAND
            elif cmd.split(' ')[0] == "download":
                if len(cmd.split(' ')[1]) >= 40:
                    self.api.getInfo(cmd.split(' ')[1])
                    self.api.download(0)
                elif cmd.split(' ')[1].isnumeric():
                    if int(cmd.split(' ')[1]) >= 0 and int(cmd.split(' ')[1]) < len(self.api.results):
                        self.api.download(int(cmd.split(' ')[1]))
                    else:
                        print("/!\\ Illegal argument /!\\")
                else:
                    print("/!\\ Illegal argument /!\\")
            # -- START COMMAND
            elif cmd.split(' ')[0] == 'start' and cmd.split(' ')[1].isnumeric():
                if int(cmd.split(' ')[1]) >= 0 and int(cmd.split(' ')[1]) < len(self.api.results):
                    if not path.exists(self.api.results[int(cmd.split(' ')[1])].hash + ".torrent"):
                        self.api.download(int(cmd.split(' ')[1]))
                    self.background_process.append(Popen(
                        [self.torrent_client_path, "{}.torrent".format(self.api.results[int(cmd.split(' ')[1])].hash)]))
            elif cmd.split(' ')[0] in ['quit', 'exit', 'bye']:
                # just to be sure that you won't see the "Illegal Command" warning while quiting :)
                pass
            else:
                print("/!\\ Illegal command /!\\")
        self.killBackgroundProcess()


if __name__ == "__main__":
    parser = parse.ArgumentParser(description="A console app to browse getstrike.net list of torrent")
    parser.add_argument("query", type=str, help="query string defining what you're looking for")
    parser.add_argument("-v", "--verbose", action='store_true',
                        help="increase verbosity")
    parser.add_argument("-s", "--sort", type=int, default=0, choices=[0, 1],
                        help="choice between different sorting methods: 0=the more seed first, 1=the best ratio seed/leech first ( nice but not implemented yet)")
    args = parser.parse_args()

    app = App(args.query, args.verbose, args.sort)
    app.run()

